#include "test.h"
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <stdio.h>

#define MIN_REGION 4800
#define SIZE 1000


void usual_allocate_memory() {
    printf("#1 Usual allocate memory test started\n");

    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* test_example = _malloc(DEFAULT_VALUE);
    debug_heap(stdout, heap);
    if (heap == NULL || test_example == NULL) {
        err("Exception. Memory wasn't allocate properly\n");
    }
    printf("Memory allocation was successful\n");

    _free(test_example);
    munmap(heap, HEAP_SIZE);
}

void free_one_of_many_blocks() {
    printf("#2 Free one of many blocks test started\n");

    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* test_example_1 = _malloc(BLOCK_SIZE);
    void* test_example_2 = _malloc(BLOCK_SIZE);
    void* test_example_3 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    _free(test_example_2);
    debug_heap(stdout, heap);

    if (!test_example_1 || !test_example_3) {
        err("Memory was damged");
    }

    _free(test_example_1);
    _free(test_example_3);
    munmap(heap, HEAP_SIZE);
}


void free_two_of_many_blocks() {
    printf("#3 Free two of many blocks test started\n");

    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* test_example_1 = _malloc(BLOCK_SIZE);
    void* test_example_2 = _malloc(BLOCK_SIZE);
    void* test_example_3 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);

    _free(test_example_2);
    printf("---------------");
    debug_heap(stdout, heap);

    if (!test_example_1 || !test_example_3) {
         err("Memory was damged");
    }

    _free(test_example_3);
    printf("---------------");
    debug_heap(stdout, heap);

    if (!test_example_1) {
        err("Memory was damged");
    }

    _free(test_example_1);
    munmap(heap, HEAP_SIZE);
}

void add_new_region_after_old() {
    printf("#4 add_new_region_after_old test started\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);

    void* test_example_1 = _malloc(BLOCK_SIZE);
    debug_heap(stdout, heap);
    printf("---------------");

    void* test_example_2 = _malloc(SIZE_BIGGER_THAN_HEAP_SIZE);
    debug_heap(stdout, heap);
    printf("---------------");

    struct block_header *header = heap;


    if (header -> capacity.bytes < REGION_MIN_SIZE * 2) {
        _free(test_example_1);
        _free(test_example_2);
        err("Heap wasn't extended\n");
    }

    if (test_example_2 == NULL) {
        _free(test_example_1);
        err("Exception. Memory wasn't allocate properly\n");
    }

    _free(test_example_1);
    _free(test_example_2);
    munmap(heap, HEAP_SIZE);
    printf("Success\n");
}

void change_address_namespace() {
    printf("#5 change address namespace test started\n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(stdout, heap);
    printf("---------------");


    void* test_example_1 = _malloc(HEAP_SIZE);
    debug_heap(stdout, heap);
    printf("---------------");

    struct block_header* test_header = heap;
    void* after_heap = mmap(test_header->contents + test_header->capacity.bytes, MIN_REGION, PROT_READ | PROT_WRITE, MAP_PRIVATE, -1, 0);

    void* test_example_2 = _malloc(SIZE);
    debug_heap(stdout, heap);
    printf("---------------");

    if (test_example_2 == NULL) {
        _free(test_example_1);
         err("Failed\n");
    }
    _free(test_example_1);
    _free(test_example_2);
    munmap(heap, HEAP_SIZE);
    munmap(after_heap, MIN_REGION);
}

void tests_call() {
    usual_allocate_memory();
    free_one_of_many_blocks();
    free_two_of_many_blocks();
    add_new_region_after_old();
    change_address_namespace();
}