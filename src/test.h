#ifndef LAB4_TEST_H
#define LAB4_TEST_H
void usual_allocate_memory(void);
void free_one_of_many_blocks(void);
void free_two_of_many_blocks(void);
void add_new_region_after_old(void);
void change_address_namespace(void);
void tests_call(void);

enum {
    HEAP_SIZE = 4000,
    DEFAULT_VALUE = 2000,
    BLOCK_SIZE = 200,
    SIZE_BIGGER_THAN_HEAP_SIZE = 5000,
};

#endif //LAB4_TEST_H
